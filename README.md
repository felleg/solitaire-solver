# Solitaire solver

This python script will deal a standard game of solitaire, and then attempt to solve it in front of your eyes.
Thanks to this, you will not need to play Solitaire ever again! :smirk:

## See it in action!
![Victory!](demo.gif)

# Usage:
```
python3 solitaire_solver.py [-h] [-p PAUSE] [-s SEED]

optional arguments:
  -h, --help            show this help message and exit
  -p PAUSE, --pause PAUSE
                        Pause time between each algorithm iteration.
  -s SEED, --seed SEED  Seed to use for randomizer.
```

# Known bugs
* The algorithm is known to get stuck in infinite loops. This is because it has no memory feature and is
  unable to know when it performs stupid moves.

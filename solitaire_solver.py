#!/bin/python3
'''
This script deals cards for a game of solitaire, then attempts to solve it in front of you
'''

import argparse
import itertools
import os
import random
import sys
import time
from termcolor import colored

LEGAL_SUITS = ['H', 'C', 'D', 'S']
CARD_COLORS = {"H":"red",
               "C":"green",
               "D":"magenta",
               "S":"cyan"}
CARD_BACK_COLOR = "blue"
DRAW_STYLE = 1

class Card:
    '''
    A playing card
    '''
    value = None
    suit = None     # H: Hearts, S: Spade, C: Club, D: Diamond
    revealed = None
    colors = None   # 0: Black, 1: Red

    def __init__(self, value, suit, revealed=0):
        assert isinstance(value, int), "ERROR: Value {} is not int".format(value)
        assert value > 0, "ERROR: Value {} is too small".format(value)
        assert value <= 13, "ERROR: Value {} is too big".format(value)

        assert suit in LEGAL_SUITS, "ERROR: Suit {} not in {}".format(suit, LEGAL_SUITS)

        assert isinstance(revealed, int), "ERROR: Revealed {} is not int".format(revealed)
        assert revealed in [0, 1], "ERROR: Revealed value ({}) should be 0 or 1".format(revealed)

        self.value = value
        self.suit = suit
        self.revealed = revealed
        self.colors = 1 if self.suit == 'H' or self.suit == 'D' else 0
    def flip(self):
        '''
        Flip a card
        '''
        self.revealed = not self.revealed
    def show_card(self):
        '''
        Return a string that can be printed (with color) to the console
        '''
        if self.revealed == 0:
            return "[{}]".format(colored('###', CARD_BACK_COLOR))

        return "[{}{}]".format(colored("{:>2}".format(self.value), CARD_COLORS[self.suit]),
                               colored(self.suit, CARD_COLORS[self.suit]))
    def print(self):
        '''
        Print the card to the console
        '''
        print(self.show_card())

class Deck:
    '''
    A deck of cards
    '''
    cards = []

    def __init__(self):
        # Create cards
        for value in range(1, 14):
            for suit in LEGAL_SUITS:
                self.cards += [Card(value, suit)]
        # Cards are shuffled
        random.shuffle(self.cards)


    def draw_cards(self, n_draw=1):
        '''
        Draw `n_draw` cards from the deck
        '''
        # Draw n cards
        drawn = []
        for _ in range(n_draw):
            if len(self.cards) > 0:
                card = self.cards.pop()
                card.flip()
                drawn += [card]
        return drawn

    # Flip cards in deck
    def flip_deck(self):
        '''
        Flip all cards in the deck.
        If individual cards have different orientations, this function will maintain them.
        '''
        for card in self.cards:
            card.flip()

    # All cards in deck face down
    def face_down(self):
        '''
        Make all cards in deck face down
        '''
        for card in self.cards:
            card.revealed = False

    # All cards in deck face up
    def face_up(self):
        '''
        Make all cards in deck face up
        '''
        for card in self.cards:
            card.revealed = True

class PlayField:
    '''
    Play field

    Contains:
        - Foundations (where cards go to earn a victory)
        - Layout (where you form columns of cards to move them around)
        - Waste (where cards from the deck are drawn to give you a boost)
    '''
    def __init__(self, n_draw):
        self.foundations = {}
        for suit in LEGAL_SUITS:
            self.foundations[suit] = []
        self.layout = {}
        for idx in range(1, 8):
            self.layout["col"+str(idx)] = []
        self.waste = []
        self.deck = Deck()
        self.n_draw = n_draw
        self.n_moves = 0

    def deal_cards(self):
        '''
        Deal the cards to enable starting the game
        '''
        # Loop over row level
        for row in range(7):
            # Loop over layout columns
            for i_col, col in enumerate(sorted(self.layout.keys())):
                if len(self.layout[col]) > i_col:
                    # Logic required to deal cards in "triangle" shape
                    continue
                face_up = bool(i_col == row)
                card = self.deck.cards.pop()
                card.revealed = face_up
                self.layout[col] += [card]

    def get_layout_length(self):
        '''
        Return the len of the longest column in the playfield
        '''
        return max([len(i) for i in self.layout.values()])

    def print(self):
        '''
        Print the playfield to the console
        '''
        # Print waste (and remaining cards in deck)
        my_string = "\t\t\t"
        if len(self.waste) > 0:
            for my_card in self.waste[-3:]:
                my_string += my_card.show_card()
        else:
            my_string += "[   ]"
        my_string += " [deck: {} {}]".format(len(self.deck.cards),
                                             "cards" if len(self.deck.cards) > 1 else "card")
        print(my_string)
        # Print foundations
        my_string = ""
        for suit in sorted(LEGAL_SUITS):
            if len(self.foundations[suit]) > 0:
                my_string += self.foundations[suit][-1].show_card()
            else:
                my_string += "[   ]"
        print(my_string)
        print() # Empty line
        # Print the play field (7 columns)
        filter_none = lambda x: 5*" " if x is None else x.show_card()
        for col1, col2, col3, col4, col5, col6, col7 in itertools.zip_longest( \
                self.layout["col1"],
                self.layout["col2"],
                self.layout["col3"],
                self.layout["col4"],
                self.layout["col5"],
                self.layout["col6"],
                self.layout["col7"]):
            mystring = \
            filter_none(col1) +\
            filter_none(col2) +\
            filter_none(col3) +\
            filter_none(col4) +\
            filter_none(col5) +\
            filter_none(col6) +\
            filter_none(col7)
            print(mystring)

    # Draw new cards into the waste pile
    def draw_waste(self):
        '''
        Draw card(s) from the waste pile
        '''
        # Check if there are enough cards in deck to continue dealing cards in
        # the waste
        if len(self.deck.cards) == 0:
            # Flip the waste into a new deck
            self.deck.cards += self.waste
            self.deck.cards.reverse()
            self.deck.face_down()
            self.waste = []

        self.waste += self.deck.draw_cards(self.n_draw)

    def search_card(self, val, suit):
        '''
        Search for a card in the playfield

        Parameters:
        -----------
        val: int
            Value of the searched card
        suit: char ['H', 'C', 'D', 'S']
            Suit of the searched card
        '''
        # Look in the layout pile
        for location in [self.layout]:
            for col in sorted(location.keys()):
                for idx_card, card in enumerate(location[col]):
                    if card.revealed:
                        if card.value == val and card.suit == suit:
                            # We found the card! Return -1 as its position if top of pile
                            if idx_card == len(location[col]) - 1:
                                return (location, col, -1)
                            return (location, col, idx_card)
        # Look in the waste pile
        for location in [self.waste]:
            for idx_card, card in enumerate(location):
                if card.revealed:
                    if card.value == val and card.suit == suit:
                        # We found the card! Return -1 as its position if top of pile
                        if idx_card == len(location) - 1:
                            return (location, -1)
        return None
        # Look in the foundations pile
        # TODO: solve why this creates infinite loops
        #for location in [self.foundations]:
        #    for col in sorted(location.keys()):
        #        if len(location[col]) > 0:
        #            top_card = location[col][-1]
        #            if top_card.value == val and top_card.suit == suit:
        #                # We found the card! Return -1 as its position if top of pile
        #                return (location, col, -1)



# Move `number_of_cards` top cards from origin to destination
def move_cards(origin, destination, number_of_cards):
    '''
    Move n cards from origin to destination

    Parameters:
    -----------
    origin:
        Where the card is moved from. Can either be the waste pile, or one of the columns of either
        the foundations or the layout.
    destination:
        Where the card is moved to. Can either be the waste pile, or one of the columns of either
        the foundations or the layout.
    number_of_cards:
        How many cards from the top of the column in the origin to move.  Usually, this number will
        be 1 (e.g., if the foundations or the waste is either the origin or the destination, this
        number must be 1 for legal play). However, when moving a set of cards from inside the layout
        to another column inside the layout, the number can go above 1.
    '''
    cards_to_move = []
    # Error handling
    if number_of_cards > len(origin):
        print("Can't move cards, n({}) > len(origin)({})".format(number_of_cards, len(origin)))
        return

    # Remove cards from origin
    for _ in range(number_of_cards):
        cards_to_move += [origin.pop()]
    cards_to_move = reversed(cards_to_move)

    # Try moving cards to destination
    try:
        destination += cards_to_move
    except Exception as moving_violation:
        print("There was an error moving cards to destination:", moving_violation)
        origin += cards_to_move

    # Flip card under the one(s) we moved
    if len(origin) > 0:
        origin[-1].revealed = True

def move_to_foundation(playfield):
    '''
    This function checks the playfield for any card that can be moved from the waste pile or the
    layout to one of the foundation columns.

    If such a card is found, it is moved to the foundation.
    '''
    success = False
    for key in playfield.foundations.keys():
        if len(playfield.foundations[key]) == 0:
            # If the foundations contain no cards, we must first place a 1 in it
            lookfor = 1
        elif playfield.foundations[key][-1].value == 13:
            # If foundation has a 13, it's complete and we can skip it
            continue
        else:
            # We are looking for a card of value +1 above the top card in this foundation column
            lookfor = playfield.foundations[key][-1].value + 1

        search_result = playfield.search_card(lookfor, key)
        if search_result is not None:
            # Is the card we want on top of some pile?
            if search_result[-1] == -1:
                if len(search_result) == 3:
                    move_cards(search_result[0][search_result[1]], playfield.foundations[key], 1)
                    playfield.n_moves += 1
                    success = True
                    break
                if len(search_result) == 2:
                    # We found the card probably in the waste (no col dimension)
                    move_cards(search_result[0], playfield.foundations[key], 1)
                    playfield.n_moves += 1
                    success = True
                    break
    return success

def move_to_layout(playfield):
    '''
    Function that checks if any card can be added to or moved inside the layout of the playfield.

    This includes :
        - moving a card from the foundation to the layout
        - moving a card from the waste pile to the layout
        - moving a card (or a set of cards) from the layout to another location in the layout
    '''
    for key in sorted(playfield.layout.keys()):
        success = None
        while success is not False:
            move_to_foundation(playfield)
            if len(playfield.layout[key]) == 0:
                # If the layour col contains no cards, we must first place a 13 in it
                lookfor = 13
                suits = LEGAL_SUITS
                random.shuffle(suits)
            elif playfield.layout[key][-1].value == 1:
                # If the layout column has an ace, we skip this column
                continue
            else:
                # We are looking for a cards of value -1 from the top card of this layout col
                lookfor = playfield.layout[key][-1].value - 1
                suits = ['C', 'S'] if playfield.layout[key][-1].colors == 1 else ['D', 'H']
                random.shuffle(suits)

            for suit in suits:
                search_result = playfield.search_card(lookfor, suit)
                if search_result is not None:
                    if len(search_result) == 3:
                        # Is the card we want on top of some pile?
                        if search_result[-1] == -1:
                            move_cards(search_result[0][search_result[1]], playfield.layout[key], 1)
                            playfield.n_moves += 1
                            success = True
                            break

                        move_cards(search_result[0][search_result[1]], playfield.layout[key],
                                   len(search_result[0][search_result[1]]) - search_result[2])
                        playfield.n_moves += 1
                        success = True
                        break
                    if len(search_result) == 2:
                        # We found the card probably in the waste (no col dimension)
                        move_cards(search_result[0], playfield.layout[key], 1)
                        playfield.n_moves += 1
                        success = True
                        break
                else:
                    success = False

def check_victory(playfield):
    '''
    Check if the game is finished (the playfield is empty)
    '''
    return playfield.get_layout_length() == 0

if __name__ == "__main__":

    # Parse execution arguments
    PARSER = argparse.ArgumentParser(description='Deal a game of Solitaire and attempt to solve it')
    PARSER.add_argument('-p', '--pause', type=float,
                        help="Pause time between each algorithm iteration.", default=0.1)
    PARSER.add_argument('-s', '--seed', type=int, help="Seed to use for randomizer.", default=None)
    ARGS = PARSER.parse_args()

    if ARGS.seed is not None:
        random.seed(ARGS.seed)

    PLAYFIELD = PlayField(DRAW_STYLE)
    PLAYFIELD.deal_cards()

    TOTALITER = 0
    VICTORY = False

    while TOTALITER < 100 and VICTORY is False:
        # See if cards can be moved to foundations
        move_to_foundation(PLAYFIELD)
        # Look in layout if a card can be moved
        move_to_layout(PLAYFIELD)
        # Draw in waste to give new opportunities
        PLAYFIELD.draw_waste()

        os.system('clear')
        print("Iterations=", TOTALITER)
        PLAYFIELD.print()

        PLAYFIELD.n_moves += 1
        TOTALITER += 1

        if check_victory(PLAYFIELD):
            VICTORY = True
            break

        time.sleep(ARGS.pause)
    if VICTORY:
        print("SOLVED! :)")
    else:
        print("OOPS, I was not able to solve this one. :(")
    print("Total moves:", PLAYFIELD.n_moves)
    sys.exit(not VICTORY)
